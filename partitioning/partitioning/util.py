"""
Utility functions and classes for interacting with files, etc.
"""

import os

import scipy.io
import scipy.sparse

import numpy
import pandas

from dataclasses import dataclass


@dataclass
class MTXDataset:
    mtx : scipy.sparse.coo_matrix
    row : numpy.ndarray
    col : numpy.ndarray


def ReadCSV(csv_filepath, skip=0, delim='\t'):
    """ Simplified CSV parser, that assumes tab-delimited columns. """
    if not os.path.isfile(csv_filepath): return None

    with open(csv_filepath, 'r') as csv_handle:
        # list or row data, if the row isn't skipped
        row_data = [
            line.strip().split(delim)
            for ndx, line in enumerate(csv_handle)
            if skip == 0 or ndx > skip
        ]

    return numpy.array(row_data)


def ReadMTX(mtx_filepath, row_filepath, col_filepath):
    return MTXDataset(
         scipy.io.mmread(mtx_filepath)
        ,ReadCSV(row_filepath)
        ,ReadCSV(col_filepath)
    )


class FSDataset:


    @classmethod
    def AddMTX(cls, name, mtx_filepath, row_filepath, col_filepath):
        pass

    def __str__(self):
        return '{} Dataset:\n\tmatrix data:\t{}\n\trow metadata:\t{}\n\tcol metadata:\t{}'.format(
             self.name
            ,self.mtx_filepath
            ,self.row_filepath
            ,self.col_filepath
        )

    def ReadMTX(self, mtx_filepath):
        """ Thin wrapper around scipy's MTX reading function. """
        return scipy.io.mmread(mtx_filepath)

    def ReadDataset(self):
        """
        Reads the dataset into memory by reading the matrix values (MTX) as a scipy coo_matrix
        (sparse, coordinate-formatted matrix).
        """

        if self.mtx_data is None:
            self.mtx_data = self.ReadMTX(self.mtx_filepath)
            self.row_data = self.ReadCSV(self.row_filepath)
            self.col_data = self.ReadCSV(self.col_filepath)

        return self

    def ReadAsMatrix(self):
        """
        Creates a pandas data frame containing the matrix data, such that there's a row for
        every gene and a column for every cell (or whatever the rows and columns represent).
        """

        self.ReadDataset()

        # Note: it is assumed that metadata files contain the primary key in the first column
        if self.mtx_matrix is None:
            self.mtx_matrix = pandas.DataFrame.sparse.from_spmatrix(
                 scipy.sparse.csc_matrix(self.mtx_data, dtype=numpy.uint16)
                ,index=self.row_data[:, 0]
                ,columns=self.col_data[:, 0]
            )

        return self.mtx_matrix

    def ReadAsRelation(self):
        """
        Creates a 3-column structure containing the matrix data, such that the first column
        contains row coordinates for matrix values, the second column contains column coordinates
        for matrix values, and the third column contains matrix values themselves. This is the same
        as the coo_matrix, but we use a pandas dataframe for consistency with 'ReadAsMatrix' and
        replace coordinates with gene names and cell IDs.
        """

        self.ReadDataset()

        # Note: it is assumed that metadata files contain the primary key in the first column
        if self.mtx_relation is None:
            row_ids  = self.row_data[:, 0][self.mtx_matrix.row]
            col_ids  = self.col_data[:, 0][self.mtx_matrix.col]
            mtx_vals = numpy.array(list(map(int, self.mtx_data.data)), dtype=numpy.uint16)

            self.mtx_relation = pandas.DataFrame(
                 data=zip(row_ids, col_ids, mtx_vals)
                ,columns=['gene', 'cell', 'expr']
            )

        return self.mtx_relation


class FSUtil:

    ebi_filename_prefix   = '.aggregated_filtered_normalised_counts'
    ebi_filename_suffixes = {
         'mtx': '_matrix.mtx'
        ,'row': '.mtx_rows'
        ,'col': '.mtx_cols'
    }

    general_filename_suffixes = {
         'mtx': '.mtx'
        ,'row': '.tsv'
        ,'col': '.tsv'
    }

    @classmethod
    def _ListDatasets(cls, dataset_rootpath, fn_finder):
        """
        Given a root directory (path to a directory containing target datasets), finds file paths
        corresponding to a dataset, for all dataset directories. For EBI datasets, a dataset
        directory is one that contains files with '.aggregated_filtered_counts' in the name (see:
        :ebi_filename_prefix:), and there should be 3 such files. The path to each file with
        :ebi_filename_prefix: in the name is then associated with the same dataset.
        """

        datasets = []

        for dirname in os.listdir(dataset_rootpath):
            dirpath = os.path.join(dataset_rootpath, dirname)

            # Pass :dirpath: to `fn_finder` to determine if :dirpath: is a dataset and find
            # associated data files.
            if os.path.isdir(dirpath):
                found_dataset = fn_finder(dirpath)
                if found_dataset: datasets.append(found_dataset)

        return datasets


    @classmethod
    def ListEBIDatasets(cls, dataset_rootpath):
        """
        Uses `_ListDatasets` to find datasets following EBI conventions.
        """

        def fn_find_ebi(dirpath):
            """
            Closure that defines how to search :dirpath: for EBI dataset files.
            """

            # get the file names to check
            target_fileset = frozenset([
                filename
                for filename in os.listdir(dirpath)
                if os.path.isfile(os.path.join(dirpath, filename))
            ])

            # determine the dataset name that :dirpath: may contain data for
            dataset_name     = os.path.basename(dirpath)
            dataset_basename = f'{dataset_name}{cls.ebi_filename_prefix}'

            # a dictionary for convenience. This could be more concise, but it'd be less readable
            mtx_filepaths = {}
            for component_key in ('mtx', 'row', 'col'):
                filepath_key = f'{component_key}_filepath'
                filepath_val = dataset_basename + cls.ebi_filename_suffixes[component_key]

                mtx_filepaths[filepath_key] = filepath_val

            # check that each filepath exists
            if any(filepath not in target_fileset for filepath in mtx_filepaths.values()):
                print(f'Could not find files for dataset "{dataset_name}"')
                return None

            # construct a container for each associated file path
            return MTXDataset(**mtx_filepaths, name=dataset_name)

        # Use closure with `_ListDatasets` to find all EBI datasets
        return cls._ListDatasets(dataset_rootpath, fn_find_ebi)


# >> Some code to test FSUtil when this script is invoked directly
if __name__ == '__main__':
    path_to_ebi_rootdirpath = '../../skyhook-kinetic-evaluation/resources/sample-data/ebi'

    ebi_datasets = FSUtil.ListEBIDatasets(path_to_ebi_rootdirpath)

    for ebi_dataset in ebi_datasets: print(ebi_dataset)
    print(len(ebi_datasets))
