import hashlib

import pyarrow


# ------------------------------
# Functions

def SkyhookSchema(attr_list):
    return pyarrow.schema(map(pyarrow.field, attr_list))


# ------------------------------
# Classes


class SkyhookPartition:
    """
    Represents a partition of a SkyhookDataset. The partition is "physical", meaning it can
    actually be found on a storage device. A partition maps 1-1 to a Ceph object, and is the atomic
    unit of autonomy in our data architecture.
    """

    def __init__(self, fn_hash=SimpleHash, **kwargs):
        super().__init__(**kwargs)

        self.schema     = None

        self.partitions = dict()


    def SetBatchSize(self, as_records, as_bytes=1048576, value_bytesize=3):
        """
        The batch size specifies how many records (aka "rows" or "observations") is contained in a
        RecordBatch of a partition. In other words, the batch size determines how many rows are in
        each sub-partition (aka "slice" or "extent") of a Dataset partition (storage object).

        If :as_records: is "False-y" (e.g. 0 or None), then :as_bytes: is used (default: 1MiB). It
        is assumed that every value in this partition will require up to :value_bytesize: bytes
        when persisted on disk (assuming lz4 compression, and homogenous data type).

        This is an attribute of a partition, because a particular partition is autonomous from
        other partitions and thus may have a distinct data geometry (in particular, number of
        attributes in the schema). Each RecordBatch can have a potentially independent physical
        design as well, but all RecordBatches of a partition must have the same logical schema
        (columns, dimension names, data types, etc.).
        """

        if as_records:
            self.batch_size = as_records

        elif not self.schema:
            err_msg = 'No schema defined for partition; batch size must be provided in records'
            sys.stderr.write(err_msg)
            sys.exit(err_msg)

        else:
            self.batch_size = as_bytes / (value_bytesize * len(self.schema))


        return self
