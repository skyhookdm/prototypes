import pyarrow


# >> Single-cell Sequencing relations
SingleCellExpression = (
    SkyhookDataset.WithName('SingleCellGeneExpression')
                  .SetDimensions(('))
                  .SetPhysicalDesign({
                        'dimensions'     : { 'row': 'cell_id', 'col': 'ensembl_gene_id' }
                       ,'data_type'      : pyarrow.uint16()
                       ,'partition_key'  : 'cluster_id'
                       ,'partition_index': HashIndex.ForDataset('SingleCellGeneExpression')
                   })
)


SingleCellClusters = (
    SkyhookDataset.WithName('SingleCellClusters')
                  .
)


class GeneSets:
    """
    Dataset containing sets of genes, where a set of genes is similar to a pathway or a gene panel.
    """

    pass
