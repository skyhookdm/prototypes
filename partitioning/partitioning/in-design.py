

class SkyhookIndex:
    """
    Common interface shared by any type of data index.
    """

    pass


class PhysicalDesign:

    @classmethod
    def WithDimensions(cls, dimension_names):
        return cls(dimension_names)

    def __init__(self, dimension_names, batch_size=20480):
        super().__init__(**kwargs)

        self.dimension_names = dimension_names
        self.batch_size      = batch_size


    def SetDimensionNames(self, dimension_names):
        """
        Dimension names represent a general, named approach to determine what data rows and columns
        represent. For tabular data that is homogeneous (aka a matrix), this allows us to pivot,
        but remember which dimension is which.

        The number of dimension names (length) should correspond to how many attributes are in the
        schema. Note, that if a partition key doesn't need to be stored with the data, that it can
        be omitted from the schema.
        """

        pass

    def SetPartitionKey(self, field_name):
        """
        The partition key specifies what data is used to identify an object containing a partition
        of this dataset. Traditionally, in databases, this must also be part of the relation.
        However, for Skyhook we can use it purely in the mechanism for identifying storage objects
        (table partitions).
        """

        pass

    def AddHashIndex(self):
        pass

    def AddRangeIndex(self):
        pass
