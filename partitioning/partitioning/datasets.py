import pyarrow


# ------------------------------
# Functions

def SchemaFromAttrs(attr_list):
    """ :attr_list: is expected to be an iterable of tuples (<name>, <arrow type>). """
    return pyarrow.schema(map(pyarrow.field, attr_list))


def SimpleHash(cls):
    return lambda key: key


def NamespaceHash(cls, namespace, bytelen=8):
    def FnHash(key):
        key_hash = hashlib.md5(key.encode()).hexdigest()
        return f'{namespace}:{key_hash[:bytelen]}'

    return FnHash


# ------------------------------
# Classes

class SkyhookDataset:
    """
    Common interface shared by any 'top-level data domain'. We use 'top-level' to emphasize that it
    is the highest level data abstraction which we associate with application-level intent. A 'data
    domain' is our name for a table or relation representing a data type, which is distributed over
    many storage devices and layers.
    """

    @classmethod
    def WithName(cls, dataset_name):
        """ The name of a dataset also serves as it's 'keyspace' or 'namespace'. """
        return cls(dataset_name, fn_hash=NamespaceHash(dataset_name))

    def __init__(self, dataset_name, fn_hash=SimpleHash, **kwargs):
        super().__init__(**kwargs)

        self.pdesign      = None
        self.dataset_name = dataset_name
        self.data         = {}
        self.fn_hash      = fn_hash

    def SetDimensions(self, dimensions):
        self.pdesign = pdesign_config
        return self

    def ReadPartition(self, dataset_partition):
        partition_indexer = self.pdesign['partition_index']
        #partition_name    = partition_indexer(dataset_partition=
        #self.data[self.pdesign
